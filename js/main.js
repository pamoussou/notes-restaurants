selectedServer = 'local';
// Init the map and trigger the local server by default
initRestaurantOnMap = new RestaurantsOnMap(selectedServer);

// Action when click on desired server
$(".maptype-selector").click(function() {
    $("#restaurants-list").html('');
    $("#restaurant-detail-container").fadeOut(100);
    selectedServer = ($(this).attr('data-trigger') && $(this).attr('data-trigger') === 'google') ?
        'google' :
        'local';
    $("#server-selected span").html(selectedServer);
    initRestaurantOnMap.setMapType(selectedServer);
});

$(document).on("click", ".single-place", function(e) {
    e.preventDefault();
    const restaurantId = $(this).attr('data-restaurantid');
    fetchRestaurantData(restaurantId);

});

// When click on close button to close restaurant detail view or restaurant adding view
$('.close-btn').click(function() {
    resetRestaurantDetailSection();
    resetNewRestaurantForm();
});

// When submit the rating adding form
$("#add-ranking-form").submit(function(e) {
    e.preventDefault();
    const valueToSave = {
        id: $("input[name=restaurantId]").val(),
        ranking: $("input[name=ranking]:checked").val(),
        comment: $("textarea[name=rankingText]").val()
    }
    const saveRanking = initRestaurantOnMap.addRanking(valueToSave);
    if (saveRanking) {
        fillComments(saveRanking);
        $("textarea[name=rankingText]").val('');
    }
});

// When submit the restaurant adding form
$("#restaurant-add-form").submit(function(e) {
    e.preventDefault();
    $(".input-error").slideUp(10);
    const restauranttNameValue = $("input[name=restaurantName]").val().trim();
    let ifError = false;
    if (!/^.{5,}$/.test(restauranttNameValue)) {
        $("input[name=restaurantName] ~ .input-error").slideDown();
        ifError = true;
    }
    if (!ifError) {
        if (initRestaurantOnMap.saveNewRestaurant(restauranttNameValue)) {
            resetNewRestaurantForm();
        } else {
            $(".main-error").fadeIn(100);
        }
    }
});

// Reset the new restaurant adding form
function resetNewRestaurantForm() {
    $(".main-error, .input-error").fadeOut(100);
    $("#restaurant-add-form-wrapper").fadeOut(100);
    $("input[name=restaurantName], input[name=restaurantAddress]").val('');
}

// Reset the restaurant detail view rating adding form
function resetRestaurantDetailSection() {
    $("#restaurant-detail-container").fadeOut(100);
    $("input[name=restaurantId]").val('');
    $("#restaurant-detail-container .restaurant-name").html('');
    $("#restaurant-detail-container .restaurant-image").html('');
    $("#restaurant-detail-container .restaurant-address").html('');
    $("#restaurant-detail-container .restaurant-ranking").html('');
    $("#restaurant-detail-container .restaurant-ranking-nb").html('');
    $("input[name=restaurantId]").val('');
    $("textarea[name=rankingText]").val('');
    $("#comments-list").html('');
}

// Show the comments on a selected restaurant
function fillComments(rastaurantData) {
    if (rastaurantData) {
        const ratings = rastaurantData.reviews;
        $("#restaurant-detail-container .restaurant-ranking").html('Note: ' + rastaurantData.ranking + ' étoiles');
        $("#restaurant-detail-container .restaurant-ranking-nb").html(rastaurantData.nranking + ' Avis');
        $("#comments-list").html('');
        let commentBuild = '';
        for (let i = 0; i < ratings.length; i++) {
            commentBuild = '<div class="comment-user-line"><div class="rating-line">Note: ' + ratings[i].rating + ' étoiles</div><div class="comment-line">' + ratings[i].text + '</div></div>';
            $("#comments-list").prepend(commentBuild);
        }
    }
}

// Retrieve restaurant data by ID
function fetchRestaurantData(restaurantId) {
    initRestaurantOnMap.restaurantData(restaurantId)
        .then(restaurantDetail => {
            if (restaurantDetail) {
                selectedServer === 'google' ? $("#add-ranking-form-wrapper").hide() : $("#add-ranking-form-wrapper").show();
                $("#restaurant-detail-container").css({ display: 'flex' }).fadeIn(100);
                $("input[name=restaurantId]").val(restaurantDetail.id);
                $("#restaurant-detail-container .restaurant-name").html(restaurantDetail.name);
                $("#restaurant-detail-container .restaurant-image").html('<img src="' + restaurantDetail.photos + '" />');
                $("#restaurant-detail-container .restaurant-address").html('Adresse: ' + restaurantDetail.address);
                fillComments(restaurantDetail);
            }
        }).catch(_ => {})

}