class RestaurantsOnMap {
    // Initiate Class properties
    constructor(mapType = 'google') {
        this.map;
        this.activeInfoWindow;
        this.markers = [];
        this.mapType = mapType;
        this.pos = {
            lat: 48.864716,
            lng: 2.349014
        };
        this.localRestaurantsInitiated;
        this.remoterestaurantsInitiated;
        this.userLocated;
        this.pendingLocation;
    }

    // Initiate the map
    initMap() {
        // Save the class instance in a value
        const instance = this;
        // If the user location is unknown, we required to accept geolocation
        if (!this.userLocated) {
            // Create the map view
            instance.map = new google.maps.Map(document.getElementById('map'), {
                center: this.pos,
                zoom: 15
            });
            // If navigator support location
            if (navigator.geolocation) {
                // If user accept geolocation
                navigator.geolocation.getCurrentPosition(function(position) {
                    instance.prepareMap();
                    // We reset the user location
                    instance.pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    instance.createMap();
                    instance.userLocated = true;
                }, () => {
                    // We show error if user didn't accept geolocation
                    instance.handleLocationError(true);
                });
            } else {
                // We show error if user browser didn't support geolocation
                instance.handleLocationError(false);
                return;
            }
        } else {
            // If user location is known the directly create the map without geolocation request
            instance.createMap();
        }
    }

    createMap() {
        // Set map center on user location
        this.map.setCenter(new google.maps.LatLng(this.pos.lat, this.pos.lng));
        // We add user marker
        this.createMarker(false, true, this.pos);
        const instance = this;
        // We set request parameters
        const request = {
            location: this.pos,
            type: ['restaurant'],
            radius: '10000',
        };
        // Action when click on the map when we are on local server
        google.maps.event.addListener(this.map, 'click', function(event) {
            if (instance.mapType === 'local') {
                // Fetch address for the clicked point on the map
                const geocoder = new google.maps.Geocoder();
                let addressFound;
                geocoder.geocode({ 'latLng': event.latLng }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) { addressFound = results[0].formatted_address; }
                        // If address exists, save the location data and show the restaurant adding form
                        if (addressFound) {
                            instance.pendingLocation = {
                                geo: { lat: event.latLng.lat(), lng: event.latLng.lng() },
                                address: addressFound
                            };
                            $("#restaurant-add-form-wrapper").fadeIn(100);
                        }
                    }
                })
            }
        });
        // If it's the local server restaurants
        if (this.mapType === 'local') {
            const restaurantsOnLocal = instance.localServerRestaurants();
            // If we have restaurants in local storage
            if (restaurantsOnLocal) {
                instance.localRestaurantsInitiated = restaurantsOnLocal;
            } else {
                // Otherwise we create and save restaurants from the local json
                instance.localRestaurantsInitiated = instance.initLocalRestaurants();
                instance.saveRestaurantsOnLocalSession(instance.localRestaurantsInitiated);
            }
            // We Add restaurants on list view
            instance.makeRestaurantsList(instance.localRestaurantsInitiated);
            for (let i = 0; i < instance.localRestaurantsInitiated.length; i++) {
                const place = instance.localRestaurantsInitiated[i];
                instance.createMarker(place);
            }
            // Set the restaurants on map
            instance.showResultsOnMap();
            // If it's the google server restaurants
        } else if (this.mapType === 'google') {
            // We fetch restaurants from google server
            new google.maps.places.PlacesService(instance.map)
                .nearbySearch(request, (results, status) => {
                    if (status == google.maps.places.PlacesServiceStatus.OK) {
                        for (let i = 0; i < results.length; i++) {
                            const place = results[i];
                            // We create markers and set them on the map
                            instance.createMarker(place);
                        }
                        // We Add restaurants on list view
                        instance.makeRestaurantsList(results);
                        // Set the restaurants on map
                        instance.showResultsOnMap();
                    };
                });
        }
    }

    // We get the pending location to save when user try to add a new restaurant
    get pendingLocationSaved() {
        return this.pendingLocation ? this.pendingLocation : null;
    }

    // We set geolocation request error text and show
    handleLocationError(browserHasGeolocation) {
        const alertMessage = browserHasGeolocation ?
            'Vous devez accepter la géolocalisation pour utiliser ce service' :
            'Votre navigateur ne supporte pas la géolocalisation';
        $("#geolocalisation-request").fadeOut(100, function() {
            $("#geolocalisation-refused .alert-message").html(alertMessage);
            $("#geolocalisation-refused").fadeIn(100);
        });
    }

    // We show loader after user accept geolocation
    prepareMap() {
        $("#geolocalisation-request").fadeOut(10, () => {
            $(this).find(".alert-message").html('');
            $("#geolocalisation-loader").fadeIn(100);
        });
    }

    // We recalculate the map and restaurants list displayers and we hide loader overlay
    showResultsOnMap() {
        $("#geolocalisation-loader").fadeOut(100, function() {
            $("#map-part").css({ 'width': '75%' });
            $("#list-part").css({ 'width': '25%' }).slideDown(100);
            $("#actions-holder").fadeOut(100);
        });
    }

    // We create the markers and display it on the map
    createMarker(place, ifUser = false, position = null) {
        const instance = this;
        // Set Infowindow content
        let messageToDisplay = ifUser ? 'Vous êtes ici' : place.name;
        messageToDisplay = '<h3 class="info-header">' + messageToDisplay + '</h3>';
        const iconAssoc = ifUser ? 'images/user-pin.png' : 'images/p-restaurant.png';
        let locattionSet;
        if (!ifUser) {
            locattionSet = place.geometry ? place.geometry.location : new google.maps.LatLng(place.coordinates.lat, place.coordinates.lng);
            const address = place.address || place.vicinity;
            if (address) { messageToDisplay = messageToDisplay + '<p class="address-line">' + address + '</p>'; };
        }
        const markerPosition = ifUser ? position : locattionSet;
        const marker = new google.maps.Marker({
            icon: iconAssoc,
            draggable: false,
            animation: google.maps.Animation.DROP,
            position: markerPosition
        });
        marker.setMap(this.map);
        if (!ifUser) {
            this.markers.push(marker);
        }
        let iwin = new google.maps.InfoWindow;
        google.maps.event.addListener(marker, 'click', function() {
            iwin.setContent(messageToDisplay);
            iwin.open(map, this);
            if (instance.activeInfoWindow) { instance.activeInfoWindow.close(); }
            instance.activeInfoWindow = iwin;
        });
        google.maps.event.addListener(marker, 'leave', function() {
            iwin.close(map, this);
        });
    }

    // Create restaurants list on the map side
    makeRestaurantsList(searchResults) {
        if (searchResults) {
            const placesFormatted = [];
            for (let i = 0; i < searchResults.length; i++) {
                const place = searchResults[i];
                const DataToDisplay = this.mapType === 'google' ? this.placeDataMapper(place) : place;
                if (DataToDisplay) {
                    $("#restaurants-list").append('<div data-restaurantid="' + DataToDisplay.id + '" class="single-place"><div class="photo-part"><img src="' + DataToDisplay.photos + '" /></div><div class="place-detail-part"><h4>' + DataToDisplay.name + '</h4><div class="nb-rankings">' + DataToDisplay.nranking + ' Avis</div> <div class="place-ranking"><span>' + DataToDisplay.ranking + '</span></div><div class="place-address">' + DataToDisplay.address + '</div></div></div>');
                    placesFormatted.push(DataToDisplay);
                }
            }
        }
    }

    // Set the market appearance animation
    toggleBounce(marker) {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }

    // Verify and map restaurant data on our data model
    placeDataMapper(placeData, wideImage = false) {
        if (placeData) {
            const imageMaxWidth = wideImage ? 1000 : 200;
            const placeName = placeData.name;
            const placePhotos = placeData.photos;
            const placeRating = placeData.rating;
            const placeNumberRating = placeData.user_ratings_total;
            const placeAddress = placeData.vicinity
            const placeServiceType = placeData.types;
            const placeCoordinates = placeData.geometry;
            const placeId = placeData.place_id;
            const placeReviews = placeData.reviews;
            if (
                placeName &&
                placePhotos &&
                placeRating &&
                placeNumberRating &&
                placeAddress &&
                placeServiceType &&
                placeCoordinates
            ) {
                const photos = (typeof placePhotos !== 'undefined') ?
                    placePhotos[0].getUrl({ 'maxWidth': imageMaxWidth }) :
                    '';
                const ratingsAssoc = placeReviews ? placeReviews : [];
                return {
                    name: placeName,
                    address: placeAddress,
                    coordinates: { lat: placeCoordinates.location.lat(), lng: placeCoordinates.location.lng() },
                    photos: photos,
                    services: placeServiceType,
                    id: placeId,
                    reviews: ratingsAssoc,
                    type: 'google',
                    ranking: placeRating,
                    nranking: placeNumberRating
                }
            }
        }
    }

    // Switch the map type
    setMapType(newType) {
        for (let i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }
        this.markers = [];
        this.mapType = newType === 'google' ? newType : 'local';
        this.initMap();
    }

    // Fetch restaurant data and send the promise back
    restaurantData(restaurantId) {
        return new Promise((resolve, reject) => {
            if (this.mapType === 'local') {
                // if it's a local restaurant, we fetch the node from data
                return resolve(this.localRestaurantsInitiated[restaurantId - 1]);
            } else if (this.mapType === 'google') {
                // If it's google restaurant we build the request and we send it to google
                // to fetch the associated restaurant
                const instance = this;
                const request = {
                    placeId: restaurantId,
                    maxWidth: '1000'
                };
                new google.maps.places.PlacesService(this.map).getDetails(request, function(place, status) {
                    if (status == google.maps.places.PlacesServiceStatus.OK) {
                        const valueToReturn = instance.placeDataMapper(place, true);
                        return resolve(valueToReturn);
                    }
                    return reject();
                });
            } else {
                return reject();
            }
        });
    }

    // Add rating and comment to the local restaurants data
    addRanking(rankingData) {
        if (rankingData && rankingData.id && rankingData.ranking && rankingData.comment) {
            const restaurantId = parseInt(rankingData.id, 0);
            if (this.localRestaurantsInitiated && this.localRestaurantsInitiated[restaurantId - 1]) {
                this.localRestaurantsInitiated[restaurantId - 1].reviews.push({
                    rating: rankingData.ranking,
                    text: rankingData.comment
                });
                this.localRestaurantsInitiated[restaurantId - 1].nranking = this.localRestaurantsInitiated[restaurantId - 1].reviews.length;
                let ratingsNumber = 0;
                for (let review of this.localRestaurantsInitiated[restaurantId - 1].reviews) {
                    const ratingtaken = review.rating
                    if (ratingtaken) { ratingsNumber = ratingsNumber + parseInt(ratingtaken, 0); }
                }
                this.localRestaurantsInitiated[restaurantId - 1].ranking = parseFloat(parseFloat(ratingsNumber / this.localRestaurantsInitiated[restaurantId - 1].nranking).toFixed(1));
                $("#restaurants-list").html('');
                this.makeRestaurantsList(this.localRestaurantsInitiated);
                this.saveRestaurantsOnLocalSession(this.localRestaurantsInitiated);
                return this.localRestaurantsInitiated[restaurantId - 1];
            }
        }
    }

    // If no restaurants found on local server we fetch local data (json) and modelize it
    initLocalRestaurants() {
        const localPlaces = this.localRestaurants();
        const latToTake = this.pos.lat;
        const lngToTake = this.pos.lng;
        let randomLat;
        let randomLng;
        // We get all the data node and create random location from the current user
        for (let i = 0; i < localPlaces.length; i++) {
            randomLat = parseFloat((Math.random() * ((latToTake + 0.0100000) - latToTake) + latToTake).toFixed(7));
            randomLng = parseFloat((Math.random() * ((lngToTake + 0.0100000) - lngToTake) + lngToTake).toFixed(7));
            localPlaces[i].coordinates = { lat: randomLat, lng: randomLng };
            localPlaces[i].type = 'local';
            localPlaces[i].nranking = localPlaces[i].reviews.length;
            const reviewsArray = localPlaces[i].reviews;
            let ratingsNumber = 0;
            for (let review of reviewsArray) {
                const ratingtaken = review.rating
                if (ratingtaken) { ratingsNumber = ratingsNumber + parseInt(ratingtaken, 0); }
            }
            // calculate the rating with the reviews number
            localPlaces[i].ranking = parseFloat(parseFloat(ratingsNumber / localPlaces[i].nranking).toFixed(1));
        }
        return localPlaces;
    }

    // Remove restaurants from local server
    resetLocalServerRestaurants() {
        const savedRestaurants = sessionStorage.getItem('restaurants');
        if (savedRestaurants) { sessionStorage.removeItem('restaurants'); }
    }

    // Get the restaurants data from local storage
    localServerRestaurants() {
        const savedRestaurants = sessionStorage.getItem('restaurants');
        if (savedRestaurants) { return JSON.parse(savedRestaurants); }
    }

    // Save the restaurants data on the local storage
    saveRestaurantsOnLocalSession(restaurantsData) {
        sessionStorage.setItem('restaurants', JSON.stringify(restaurantsData));
    }

    // Get the local restaurants Array Length
    get restaurantsLength() {
        return this.localRestaurantsInitiated ? this.localRestaurantsInitiated.length : 0;
    }

    // Save the new restaurant, save it and refresh the side restaurant list
    saveNewRestaurant(restauranttNameValue) {
        // Verify new restaurant data
        if (restauranttNameValue) {
            const existingRestaurants = (this.restaurantsLength === 0) ? 1 : this.restaurantsLength + 1;
            const restaurantImage = 'images/' + Math.floor(Math.random() * (10 - 1) + 1) + '.jpg';
            const restaurantdata = {
                name: restauranttNameValue,
                coordinates: this.pendingLocation.geo,
                address: this.pendingLocation.address,
                photos: restaurantImage,
                services: 'restaurant',
                id: existingRestaurants,
                ranking: 0,
                nranking: 0,
                reviews: []
            }
            this.localRestaurantsInitiated.push(restaurantdata);
            this.saveRestaurantsOnLocalSession(this.localRestaurantsInitiated);
            this.makeRestaurantsList(this.localRestaurantsInitiated);
            this.createMarker(restaurantdata);
            this.pendingLocation = null;
            return true;
        }
    }

    // Return local restaurants data
    localRestaurants() {
        return [{
                name: "Amadi Boa",
                address: "12 rue de la place",
                photos: "images/1.jpg",
                services: 'restaurant',
                id: 1,
                ranking: 4,
                nranking: 0,
                reviews: [{
                        "rating": 5,
                        "text": "C'était un coup de coeur"
                    },
                    {
                        "rating": 3,
                        "text": "C'était bien mais le service est à améliorer"
                    }
                ]
            },
            {
                name: "Au tacos du roi",
                address: "1 Rue Franche comté",
                photos: "images/2.jpg",
                services: 'restaurant',
                id: 2,
                ranking: 2.5,
                nranking: 0,
                reviews: [{
                        "rating": 2,
                        "text": "Le restaurant était un peu sal"
                    },
                    {
                        "rating": 3,
                        "text": "Vous devez fermer et améliorer beaucoup de choses"
                    }
                ]
            },
            {
                name: "Pizza d'italie",
                address: "22 Bis allée des cèdres",
                photos: "images/3.jpg",
                services: 'restaurant',
                id: 3,
                ranking: 2.5,
                nranking: 0,
                reviews: [{
                        "rating": 1,
                        "text": "Une minuscule pizzeria délicieuse cachée juste à côté du Sacré choeur !"
                    },
                    {
                        "rating": 5,
                        "text": "J'ai trouvé ça correct, sans plus"
                    }
                ]
            },
            {
                name: "Tiger house",
                address: "9 Ter Route de Vilaine",
                photos: "images/4.jpg",
                services: 'restaurant',
                id: 4,
                ranking: 2.5,
                nranking: 0,
                reviews: [{
                        "rating": 3,
                        "text": "Un excellent restaurant, j'y reviendrai ! Par contre il vaut mieux aimer la viande."
                    },
                    {
                        "rating": 3,
                        "text": "Tout simplement mon restaurant préféré !"
                    }
                ]
            }, {
                name: "Riz d'Asie",
                address: "5 Square des reines",
                photos: "images/5.jpg",
                services: 'restaurant',
                id: 5,
                ranking: 2.5,
                nranking: 0,
                reviews: [{
                        "rating": 5,
                        "text": "Une minuscule pizzeria délicieuse cachée juste à côté du Sacré choeur !"
                    },
                    {
                        "rating": 3,
                        "text": "J'ai trouvé ça correct, sans plus"
                    }
                ]
            }, {
                name: "Le Phénix epic",
                address: "15 rue du fervre",
                photos: "images/6.jpg",
                services: 'restaurant',
                id: 6,
                ranking: 2.5,
                nranking: 0,
                reviews: [{
                        "rating": 2,
                        "text": "Une minuscule pizzeria délicieuse cachée juste à côté du Sacré choeur !"
                    },
                    {
                        "rating": 4,
                        "text": "J'ai trouvé ça correct, sans plus"
                    }
                ]
            }, {
                name: "Les alizées du lac",
                address: "7 Impasse de la chapelle",
                photos: "images/7.jpg",
                services: 'restaurant',
                id: 7,
                ranking: 2.5,
                nranking: 0,
                reviews: [{
                        "rating": 1,
                        "text": "Une minuscule pizzeria délicieuse cachée juste à côté du Sacré choeur !"
                    },
                    {
                        "rating": 2,
                        "text": "J'ai trouvé ça correct, sans plus"
                    }
                ]
            }
        ];
    }
}